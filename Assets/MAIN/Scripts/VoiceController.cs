﻿using System.Collections;
using System.Collections.Generic;
using TextSpeech;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Android;

public class VoiceController : MonoBehaviour
{
    public string LANG_CODE;
    public string speakMessage;

    [SerializeField]
    Text uiText;
    [SerializeField]
    private GameObject[] allChecks;

    

    private void Start()
    {
        ChangeLenguage(0);

#if UNITY_ANDROID
        SpeechToText.instance.onPartialResultsCallback = OnPartialSpeechResult;
#endif
        SpeechToText.instance.onResultCallback = OnFinalSpeechResult;
        TextToSpeech.instance.onStartCallBack = OnSpeakStart;
        TextToSpeech.instance.onDoneCallback = OnSpeakStop;

        CheckPermission();
    }

    void CheckPermission()
    {
#if UNITY_ANDROID
        if(!Permission.HasUserAuthorizedPermission(Permission.Microphone))
        {
            Permission.RequestUserPermission(Permission.Microphone);
        }
#endif
    }

    #region Text to Speech

    public void StartSpeaking()
    {
        TextToSpeech.instance.StartSpeak(speakMessage);
    }

    public void StopSpeaking()
    {
        TextToSpeech.instance.StopSpeak();
    }

    void OnSpeakStart()
    {
        Debug.Log("Talking started...");
    }

    void OnSpeakStop()
    {
        Debug.Log("Talking stopped");
    }

    #endregion

    #region Speech to Text

    public void StartListening()
    {
        SpeechToText.instance.StartRecording();
    }

    public void StopListening()
    {
        SpeechToText.instance.StopRecording();
    }

    void OnFinalSpeechResult(string result)
    {
        uiText.text = result;
    }

    void OnPartialSpeechResult(string result)
    {
        uiText.text = result;
    }

    #endregion

    void Setup(string code)
    {
        TextToSpeech.instance.Setting(code, 1, 1);
        SpeechToText.instance.Setting(code);
    }

    public void ChangeLenguage(int lenguage)
    {
        switch (lenguage)
        {
            case 0:
                print("usa");
                LANG_CODE = "en-US";
                Setup(LANG_CODE);
                EnableCheck(0);
                break;

            case 1:
                print("col");
                LANG_CODE = "es-CO";
                Setup(LANG_CODE);
                EnableCheck(1);
                break;
        }
    }

    private void EnableCheck(int check)
    {
        print("OK");
        for (int i = 0; i < allChecks.Length; i++)
        {
            allChecks[i].SetActive(false);
        }

        allChecks[check].SetActive(true);
    }
}
