﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class AutoFocusCamera : MonoBehaviour {

    #region Public_Variables
    public bool canFocus = true;
    #endregion //Public_Variables

    #region Private_Variables


    #endregion //Private_Variables


    #region Unity_MonoBehaviour

    //AutoFocus
    void Start()
    {
        var vuforia = VuforiaARController.Instance;
        vuforia.RegisterVuforiaStartedCallback(OnVuforiaStarted);
        vuforia.RegisterOnPauseCallback(OnPaused);
    }
    
    //Touch to focus
    private void Update()
    {
        if (Input.touchCount == 1 && canFocus)
        {
            canFocus = false;
            FocusOnTouch();
        }
    }

    #endregion //Unity_MonoBehaviour


    #region Public_Methods
    #endregion //Public_Methods

    #region Private_Methods

    private void OnVuforiaStarted()
    {
        CameraDevice.Instance.SetFocusMode(
            CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    private void OnPaused(bool paused)
    {
        if (!paused)
        {

            CameraDevice.Instance.SetFocusMode(
                CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        }
    }

    private void FocusOnTouch()
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
        Invoke("ResetFocus", 2f);
    }

    private void ResetFocus()
    {
        canFocus = true;
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    #endregion //Private_Methods
}
