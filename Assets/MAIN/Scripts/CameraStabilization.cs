﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraStabilization : MonoBehaviour
{
    #region Public_Variables

    [Tooltip("Main Image target")]
    public Transform imgTargetTrasform;
    [Range(1f,25f)]
    [Tooltip("Low values means high smoothness")]
    public float smoothnessFactor = 25f;
    public bool isTrackingEnable = true;
    public bool isStabilizating = true;
    public bool isDistanceFiltering = true;

    #endregion //Public_Variables

    #region Private_Variables
    [Tooltip("Used to calculate the shoothnessFactor when isDistanceFiltering is true")]
    private float[] inputRange = new float[2] { 0.5f, 6 };

    [Tooltip("Used to calculate the shoothnessFactor when isDistanceFiltering is true")]
    private float[] outputRange = new float[2] { 1f, 25 };



    private Vector3 lastPosition;
    private Quaternion lastRotation;

    #endregion //Private_Variables


    #region Unity_MonoBehaviour

    private void OnEnable()
    {
        lastPosition = transform.localPosition;
        lastRotation = transform.localRotation;
    }

    private void LateUpdate()
    {
        if (isTrackingEnable && isDistanceFiltering)
        {
            float _distance = Vector3.Distance(transform.position, imgTargetTrasform.position);
            smoothnessFactor = ConvertRange(_distance, inputRange, outputRange);
        }
        else
        {
            smoothnessFactor = 15f;
        }

        if (isTrackingEnable && isStabilizating)
        {
            transform.localPosition = Vector3.Lerp(lastPosition, transform.localPosition, Time.deltaTime * smoothnessFactor);
            transform.localRotation = Quaternion.Lerp(lastRotation, transform.localRotation, Time.deltaTime * smoothnessFactor);

            lastPosition = transform.localPosition;
            lastRotation = transform.localRotation;

        }
    }

    #endregion // Unity_Monobehaviour


    #region Public_Methods

    #endregion //Public_Methods

    #region Private_Methods

    /// <summary>
    /// Normalize a value beetween two factors
    /// </summary>
    /// <param name="pVal">Value to be normalized</param>
    /// <param name="pInputRange">Range in whete the pVal can be</param>
    /// <param name="pOutputRange">Range to be normalized</param>
    /// <returns>pVal that is normalized</returns>
    private float ConvertRange(float pVal, float[] pInputRange, float[] pOutputRange)
    {
        return (pVal - pInputRange[0]) * (pOutputRange[1] - pOutputRange[0]) / (pInputRange[1] - pInputRange[0]) + pOutputRange[0];
    }

    #endregion //Prevate_Variables















}