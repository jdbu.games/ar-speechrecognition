﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllCamerasController : MonoBehaviour
{
    [SerializeField]
    private GameObject arCamera;
    [SerializeField]
    private GameObject normalCamera;
    [SerializeField]
    private GameObject arDisableIcon;
    [SerializeField]
    private GameObject room;
    [SerializeField]
    private bool arEnable;

    private void Start()
    {
        arEnable = true;
    }

    public void ChangeCamera()
    {
        arEnable = !arEnable;

        if(arEnable == true)
        {
            arCamera.SetActive(true);
            normalCamera.SetActive(false);
            arDisableIcon.SetActive(false);
            room.SetActive(false);
        }
        else
        {
            normalCamera.SetActive(true);
            arCamera.SetActive(false);
            arDisableIcon.SetActive(true);
            room.SetActive(true);
        }
    }
}
