﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotController : MonoBehaviour
{
    [SerializeField]
    private GameObject robotContainer;
    [SerializeField]
    private GameObject robotPrefab;
    [SerializeField]
    private GameObject targetReference;
    [SerializeField]
    private VoiceController voiceController;
    [SerializeField]
    private string[] greetingMessages;
    [SerializeField]
    private bool wasActivated;

    private MeshCollider meshColliderTargetReference;

    private void Start()
    {
        meshColliderTargetReference = targetReference.GetComponent<MeshCollider>();
    }

    private void Update()
    {
        if(meshColliderTargetReference.enabled == true)
        {
            if(wasActivated == false)
            {
                EnableGreeting();
                wasActivated = true;
            }

            robotPrefab.SetActive(true);
            robotContainer.transform.position = targetReference.transform.position;
        }
        else
        {
            robotContainer.transform.position = new Vector3(0, -2, -1f);
            robotContainer.transform.rotation = Quaternion.Euler(90, 0, 0);
        }
    }

    private void EnableGreeting()
    {
        if(voiceController.LANG_CODE == "en-US")
        {
            voiceController.speakMessage = greetingMessages[0];
        }
        else if (voiceController.LANG_CODE == "es-CO")
        {
            voiceController.speakMessage = greetingMessages[1];
        }

        voiceController.StartSpeaking();
    }
}
